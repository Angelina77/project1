# project1

Continuous Delivory of mini project1

## Deployed at Netlify

https://vocal-puppy-85a7dc.netlify.app

- Create a Netlify account and link it to your GitHub account
- Choose the repository you want to deploy
- Configure the build settings
![image](build.png)
- Deploy the site


